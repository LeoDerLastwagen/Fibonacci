package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class Main {
    private final static String WRONG_INPUT_MESSAGE = "Your input has to be an Integer greater or equal to zero (or empty / enter to finish)";
    private final static String CONSOLE_ERROR_MESSAGE = "An error occurred while reading from the console";

    public static void main(String[] args) {
        calculateFibonacciNumbers();
    }

    // Calculates the fibonacci number for each line with a number in the console.
    // Outputs the results when entering empty.
    private static void calculateFibonacciNumbers() {
        // setup
        LinkedList<String> results = new LinkedList<>();
        MyMath myMath = new MyMath();
        BufferedReader input = new BufferedReader(
                new InputStreamReader(System.in));
        int n;

        // read input from console and calculate results
        boolean repeat = true;
        while(repeat) {
            try {
                String line = input.readLine();
                if (line.isBlank())
                    repeat = false;
                else {
                    n = Integer.parseInt(line.trim());
                    results.add("Die Fibonacci Zahl für " + n + " ist: " + myMath.calculateFibonacci(n));
                }
            }
            catch (IllegalArgumentException e) {
                results.add(WRONG_INPUT_MESSAGE);
            }
            catch (IOException e) {
                repeat = false;
                System.out.println();
                results.add(CONSOLE_ERROR_MESSAGE);
            }
        }

        // output results
        for (String result : results) {
            System.out.println(result);
        }
    }
}