package org.example;

import java.math.BigInteger;

public class MyMath {

    // Calculates the Fibonacci number F(n)
    // F(0)=0, F(1)=1, F(n)=F(n-2)+F(n-1)
    // e.g. F(2)=F(0)+F(1)=0+1=1
    // Throws IllegalArgumentException if n is lower than zero
    public BigInteger calculateFibonacci(int n) {
        if (n < 0)
            throw new IllegalArgumentException("n has to be greater or equal to zero");
        BigInteger f0 = new BigInteger("0"); // F(i-2)
        BigInteger f1 = new BigInteger("1"); // F(i-1)
        BigInteger result = n == 0 ? new BigInteger("0") : new BigInteger("1"); // for the case n=0 or n=1
        for (int i = 2; i <= n; ++i) {
            result = f0.add(f1); // F(i) = F(i-2) + F(i-1)
            f0 = f1; // F(i-1) (fun fact: the "=" deep copies because BigInteger is immutable)
            f1 = result; // F(i)
        }
        return result;
    }
}
