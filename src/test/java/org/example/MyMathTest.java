package org.example;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class MyMathTest {


    @ParameterizedTest
    @CsvSource({"0,0", "1,1", "5,5", "7,13", "11,89", "50,12586269025", "250,7896325826131730509282738943634332893686268675876375"})
    void calculateFibonacci_ValidInput_ReturnsRightValues(int input, BigInteger expectedResult) {
        MyMath myMath = new MyMath();
        assertEquals(expectedResult, myMath.calculateFibonacci(input));
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, -2, -3, -10, -5000})
    void calculateFibonacci_NegativeInput_ThrowsException(int input) {
        MyMath myMath = new MyMath();
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> myMath.calculateFibonacci(input));
        assertEquals("n has to be greater or equal to zero", exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 1, 42, 500, 5000})
    void calculateFibonacci_600Times_LessThan30Seconds(int input) {
        MyMath myMath = new MyMath();
        long start = System.currentTimeMillis();

        for (int i = 0; i < 600; ++i) {
            myMath.calculateFibonacci(input);
        }

        long secondsTaken = (System.currentTimeMillis() - start) / 1000;
        assertTrue(secondsTaken < 30);
    }
}